from django.apps import AppConfig
from django.contrib import admin

# Register your models here.

# organization/adminx.py

import xadmin

from .models import CityDict, CourseOrg, Teacher
from django.utils.safestring import mark_safe
from django.contrib import admin


from django.utils.safestring import mark_safe
from django.contrib import admin


class CityDictAdmin(object):
    """ 城市信息 """
    list_display = ['name', 'desc', 'add_time']
    search_fields = ['name', 'desc']
    list_filter = ['name', 'desc', 'add_time']


class CourseOrgAdmin(object):
    """課程機構基本信息"""
    list_display = ['name', 'desc', 'click_nums', 'fav_nums', 'add_time', 'image_']
    search_fields = ['name', 'desc', 'click_nums', 'fav_nums']
    list_filter = ['name', 'desc', 'click_nums', 'fav_nums', 'city__name', 'address', 'add_time']


class TeacherAdmin(object):
    """教師基本信息"""
    list_display = ['name', 'org', 'work_years', 'work_company', 'add_time']
    search_fields = ['org', 'name', 'work_years', 'work_company']
    list_filter = ['org__name', 'name', 'work_years', 'work_company', 'click_nums', 'fav_nums', 'add_time']


xadmin.site.register(CityDict, CityDictAdmin)
xadmin.site.register(CourseOrg, CourseOrgAdmin)
xadmin.site.register(Teacher, TeacherAdmin)
