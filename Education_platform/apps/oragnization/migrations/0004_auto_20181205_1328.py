# Generated by Django 2.0.1 on 2018-12-05 05:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('oragnization', '0003_auto_20181205_1144'),
    ]

    operations = [
        migrations.AlterField(
            model_name='citydict',
            name='name',
            field=models.CharField(max_length=20, verbose_name='城市'),
        ),
        migrations.AlterField(
            model_name='courseorg',
            name='city',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='oragnization.CityDict', verbose_name='所在城市'),
        ),
    ]
