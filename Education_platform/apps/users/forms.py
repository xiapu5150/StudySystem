# -*- encoding:utf-8 -*-
# Author:Alano
import re
from captcha.fields import CaptchaField
from django import forms
from django.core.exceptions import ValidationError

from users.models import UserProfile


def iphone_validators(value):
    """手機號碼驗證"""
    mobile_re = re.compile(r'^(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$')
    if not mobile_re.search(value):
        raise ValidationError('手机号码格式错误')


class LoginForm(forms.Form):
    """登錄表單驗證"""
    username = forms.CharField(required=True,
                               error_messages={'required': '账号不能为空'})

    password = forms.CharField(required=True, min_length=6, max_length=20,
                               error_messages={'required': '密码不能为空',
                                               'min_length': '密码不能少于6位字符',
                                               'max_length': '密码不能大于20位字符'})


class RegisterFrom(forms.Form):
    """注册验证表单"""
    email = forms.EmailField(required=True,
                             error_messages={'required': '邮箱不能为空',
                                             'invalid': '邮箱格式不符合'})
    password = forms.CharField(required=True,
                               # min_length=6,
                               # max_length=20,
                               # error_messages={'required': '密码不能为空',
                               #                 'min_length': '密码不能少于6位字符',
                               #                 'max_length': '密码不能大于20位字符',}
                               )
    # 验证码
    captcha = CaptchaField(required=True, error_messages={'required': '验证码不能为空'})


class ForgetPwdForm(forms.Form):
    """忘記密碼"""
    email = forms.EmailField(required=True)
    captcha = CaptchaField(error_messages={'required': '验证码错误'})


class ModifyPwdForm(forms.Form):
    """修改密碼"""
    password1 = forms.CharField(required=True, min_length=6, max_length=20, error_messages={
                                                                           'required': '字段不能为空',
                                                                           'min_length': '不能小于6位密码',
                                                                           'max_length': '密码不能超过20位'})

    password2 = forms.CharField(required=True, min_length=6,  max_length=20, error_messages={
                                                                           'required': '字段不能为空',
                                                                           'min_length': '不能小于6位密码',
                                                                           'max_length': '密码不能超过20位'})


class UploadImageForm(forms.ModelForm):
    """用户更改图像"""
    class Meta:
        model = UserProfile
        fields = ['image']


class UserInfoForm(forms.ModelForm):
    """个人中心信息修改"""
    class Meta:
        model = UserProfile
        fields = ['nick_name','gender','birthday','address','mobile']


