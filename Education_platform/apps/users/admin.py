from django.contrib import admin

# Register your models here.
import xadmin
from xadmin import views

from .models import EmailVerifyRecord, Banner, UserProfile
from xadmin.views.base import CommAdminView


class BaseSetting(object):
    # 开启主题功能
    enable_themes = True
    use_bootswatch = True

# 将基本配置管理与view绑定
xadmin.site.register(views.BaseAdminView,BaseSetting)


class GlobalSetting(object):
    # 设置顶部后台标题信息
    site_title = '后台管理系统标题'
    # 设置底部信息
    site_footer = '后台的底部信息'
    # 设置菜单可折叠
    menu_style = "accordion"

xadmin.site.register(views.CommAdminView, GlobalSetting)


# xadmin中这里是继承object，不再是继承admin
class EmailVerifyRecordAdmin(object):
    # 显示的列
    list_display = ['code', 'email', 'send_type', 'send_time']
    # 搜索的字段，不要添加时间搜索
    search_fields = ['code', 'email', 'send_type']
    # 过滤
    list_filter = ['code', 'email', 'send_type', 'send_time']


class BannerAdmin(object):
    list_display = ['title', 'image', 'url', 'index', 'add_time']
    search_fields = ['title', 'image', 'url', 'index']
    list_filter = ['title', 'image', 'url', 'index', 'add_time']


class UserProfileAdmin(object):
    list_display = ['nick_name', 'birthday', 'gender', 'mobile', 'adress']
    search_fields = ['nick_name', 'gender', 'mobile']
    list_filter = ['nick_name', 'mobile', 'birthday', 'gender', 'adress']


xadmin.site.register(EmailVerifyRecord, EmailVerifyRecordAdmin)
xadmin.site.register(Banner, BannerAdmin)
