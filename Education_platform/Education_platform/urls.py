"""Education_platform URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf.urls import url
from django.views.static import serve

import xadmin
from django.views.generic import TemplateView
from users import views
from users.views import ActiveUserView, ResetView, ForgetPwdView, ModifyPwdView, LogoutView, LoginUnsafeView
from oragnization.views import OrgView

urlpatterns = [
    path('^login/', LoginUnsafeView.as_view(), name='login'),
    path('xadmin/', xadmin.site.urls),
    # path('', TemplateView.as_view(template_name='index.html'), name='index'),
    path('', views.IndexView.as_view(), name='index'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name="logout"),
    path('register/', views.RegisterView.as_view(), name='register'),
    path('captcha/', include('captcha.urls')),
    re_path('active/(?P<active_code>.*)/', ActiveUserView.as_view(), name='user_active'),
    re_path('reset_pwd/(?P<active_code>.*)/', ResetView.as_view(), name='user_reset_pwd'),
    path('forget/', ForgetPwdView.as_view(), name='forget_pwd'),
    path('modify_pwd/', ModifyPwdView.as_view(), name='modify_pwd'),
    # path('org_list/', OrgView.as_view(), name='org_list'),
    path("org/", include('oragnization.urls', namespace="org")),
    path("course/", include('course.urls', namespace="course")),
    path("users/", include('users.urls', namespace="users")),
    re_path(r'^media/(?P<path>.*)', serve, {"document_root": settings.MEDIA_ROOT}),
    re_path(r'^static/(?P<path>.*)', serve, {"document_root": settings.STATIC_ROOT}),
]

handler404 = 'users.views.page_not_found'
handler500 = 'users.views.page_error'
